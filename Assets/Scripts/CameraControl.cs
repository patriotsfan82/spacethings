﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

  public GameObject followTarget;
	
	// Update is called once per frame
	void LateUpdate () {
    transform.position = new Vector3 (followTarget.transform.position.x, followTarget.transform.position.y, transform.position.z);
	}
}