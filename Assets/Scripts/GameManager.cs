﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

using System.Collections.Generic;		//Allows us to use Lists. 
using UnityEngine.UI;					//Allows us to use UI.

public class GameManager : MonoBehaviour
{

	public static GameManager instance = null;				//Static instance of GameManager which allows it to be accessed by any other script
	
//	private Text levelText;									//Text to display current level number.
//	private GameObject levelImage;							//Image to block out level as levels are being set up, background for levelText.
	private int level = 1;									//Current level number, expressed in game as "Day 1".
	private bool enemiesMoving;								//Boolean to check if enemies are moving.
	private bool doingSetup = true;							//Boolean to check if we're setting up board, prevent Player from moving during setup.
	
  List<Planet> planets = new List<Planet>();
  List<GravWell> gravWells = new List<GravWell>();

	//Awake is always called before any Start functions
	void Awake()
	{
    //Check if instance already exists
    if (instance == null)

      //if not, set instance to this
      instance = this;

    //If instance already exists and it's not this:
    else if (instance != this)

      //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
      Destroy(gameObject);	
		
		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);


		
		//Call the InitGame function to initialize the first level 
		InitGame();
	}

      //this is called only once, and the paramter tell it to be called only after the scene was loaded
      //(otherwise, our Scene Load callback would be called the very first load, and we don't want that)
      [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
      static public void CallbackInitialization()
      {
          //register the callback to be called everytime the scene is loaded
          SceneManager.sceneLoaded += OnSceneLoaded;
      }

      //This is called each time a scene is loaded.
      static private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
      {
          instance.level++;
          instance.InitGame();
      }

	
	//Initializes the game for each level.
	void InitGame()
	{
//		//While doingSetup is true the player can't move, prevent player from moving while title card is up.
//		doingSetup = true;
//		
//		//Get a reference to our image LevelImage by finding it by name.
//		levelImage = GameObject.Find("LevelImage");
//		
//		//Get a reference to our text LevelText's text component by finding it by name and calling GetComponent.
//		levelText = GameObject.Find("LevelText").GetComponent<Text>();
//		
//		//Set the text of levelText to the string "Day" and append the current level number.
//		levelText.text = "Day " + level;
//		
//		//Set levelImage to active blocking player's view of the game board during setup.
//		levelImage.SetActive(true);
//		
//		//Call the HideLevelImage function with a delay in seconds of levelStartDelay.
//		Invoke("HideLevelImage", levelStartDelay);		
	}
	
	
	//Hides black image used between levels
	void HideLevelImage()
	{
//		//Disable the levelImage gameObject.
//		levelImage.SetActive(false);
//		
//		//Set doingSetup to false allowing player to move again.
//		doingSetup = false;
	}
	
	//Update is called every frame.
	void Update()
	{
    //W hen a key is pressed down it see if it was the escape key if it was it will execute the code
    if(Input.GetKeyDown("escape")) {
      Application.Quit(); // Quits the game
    }

    // Continue along...
    if (Input.GetMouseButton (0)) {
      if (Time.timeScale == 1.0f)
        Time.timeScale = 0.1f;
    } else {
      Time.timeScale = 1.0f;
    }
	}
	
  public void RegisterPlanet(Planet planet)
  {
    if (!planets.Contains (planet))
      planets.Add (planet);
  }

  public void RegisterGravWell(GravWell gravWell)
  {
    if (!gravWells.Contains(gravWell))
    {
      gravWells.Add(gravWell);
    }
  }

  public List<GravWell> GravWellsNearPoint(Vector3 point)
  {
    List<GravWell> returnList = new List<GravWell>();

    // Rework planets such that gravity and the accompanyhing colliders
    // are unique game objects.. then update most of this stuff appropriately.
    foreach (GravWell gravWell in gravWells) {
      CircleCollider2D collider = gravWell.GetComponent<CircleCollider2D> ();
      if (collider != null &&  collider.bounds.Contains (point)) {
        returnList.Add (gravWell);
      }
    }

    return returnList;
  }

  // Used when we ant to reset the origin. Pretty simple, just adjust the position of everything
  public void ResetOrigin(Vector3 offset)
  {
    foreach (Planet planet in planets) {
      Vector3 pos = planet.transform.position;
      pos += offset;
      planet.transform.position = pos;
    }

    foreach (GravWell gravWell in gravWells) {
      Vector3 pos = gravWell.transform.position;
      pos += offset;
      gravWell.transform.position = pos;
    }

    BackgroundManager.instance.ResetOrigin (offset);
  }
	
	//GameOver is called when the player reaches 0 food points
	public void GameOver()
	{
//		//Set levelText to display number of levels passed and game over message
//		levelText.text = "After " + level + " days, you starved.";
//		
//		//Enable black background image gameObject.
//		levelImage.SetActive(true);
//		
//		//Disable this GameManager.
//		enabled = false;
	}
}