﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour {
  
  public GameObject backgroundFab;
  public static BackgroundManager instance = null;
 
  // Hold some references to our tiles for easier access
  private List<GameObject> allTiles = new List<GameObject> ();
  private GameObject center = null;
  private Transform spaceObject = null;

  private float tileWidth = 0f;
  private float tileHeight = 0f;
  private const float triggerHeightBuffer = 1f;
  private const float triggerWidthBuffer = 1f;

  //Awake is always called before any Start functions
  void Awake()
  {
    //Check if instance already exists
    if (instance == null)
      instance = this;

    //If instance already exists and it's not this:
    else if (instance != this)
      Destroy(gameObject);  

    //Sets this to not be destroyed when reloading scene
    DontDestroyOnLoad(gameObject);

    // To make later calculations easier, get the width and height of our prefab here
    SpriteRenderer sprite = backgroundFab.GetComponent<SpriteRenderer>();
    if (sprite != null) {
      tileWidth = sprite.bounds.max.x - sprite.bounds.min.x;
      tileHeight = sprite.bounds.max.y - sprite.bounds.min.y;
    }

    initBackgrounds ();
  }

  // To begin, this script will contain methods for managing a set of
  // 9 background tile sprites that will be infinitely resused.
  public void initBackgrounds()
  {
    //Instantiate Board and set boardHolder to its transform.
    spaceObject = new GameObject ("Space").transform;

    // This implementation is using a 3x3 array of background tiles
    // that will dynamically moved/recyled over time.
    for (int x = -1; x <= 1; x++)
    {
      for (int y = -1; y <= 1; y++)
      {
        GameObject temp = CreateBGTile (backgroundFab, x, y);
        allTiles.Add (temp);

        temp.transform.parent = spaceObject; // Setting parent for hierarchy purposes.. 
                                             // Still dont entirely know what I'm doing here

        if (x == 0 && y == 0) {
          center = temp;
        }
      }
    }
  }

  // xPos/yPos should be -1, 0, or 1 to indicate which position
  // the tile should be put in the 3x3 array
  // this method is also assuming that all tiles are the same size
  private GameObject CreateBGTile (GameObject bg, int xPos, int yPos)
  {
    float newX = 0f, newY = 0f;

    // Have height and width now, calculate midpoint and place there.
    // For upper Left, x is negative, y is positive.
    newX = xPos * tileWidth; // -width, 0, or width
    newY = yPos * tileHeight; // -height, 0, or height

    return Instantiate(bg, new Vector3 (newX, newY, 0f), Quaternion.identity) as GameObject;
  }

  // Our primary parallax method. Takes the player start and end position and
  // moves every planet an appropriate amount to give the impression that they
  // are moving at a different rate than the background
  public void PlayerMoved (Vector3 initPos, Vector3 endPos)
  {
    Vector3 moved = endPos - initPos;
    moved *= 0.2f;

    if (initPos != endPos) {
      foreach (GameObject tile in allTiles) {
        // Determine if we moved away from or towards planet
        float distanceStart = (initPos - tile.transform.position).magnitude;
        float distanceEnd = (endPos - tile.transform.position).magnitude;

        tile.transform.position += moved;
      }
    }

    // After adjusting for parallax, need to determine if we want to reset the background item positions.
    // If the ship is far enough away from the center of the center tile, need to shift all tiles. Check
    // both height and width independantly.
    if (Mathf.Abs(endPos.y - center.transform.position.y) >= ((tileHeight / 2) + triggerHeightBuffer)) {
      // Since we have passed the trigger in y, lets shift everything up/down as needed.

      float modifier = (endPos.y - center.transform.position.y > 0) ? 1f : -1f;
      foreach (GameObject bg in allTiles) {
        Vector3 pos = bg.transform.position;
        pos.y += (modifier * tileHeight);
        bg.transform.position = pos;
      }
    }

    if (Mathf.Abs(endPos.x - center.transform.position.x) >= ((tileWidth / 2) + triggerWidthBuffer)) {
      float modifier = (endPos.x - center.transform.position.x > 0) ? 1f : -1f;

      // Since we have passed the trigger in x, lets shift everything up/down as needed.
      foreach (GameObject bg in allTiles) {
        Vector3 pos = bg.transform.position;
        pos.x += (modifier * tileWidth);
        bg.transform.position = pos;
      }
    }
  }

  // Used when we ant to reset the origin. Pretty simple, just adjust the position of everything
  public void ResetOrigin(Vector3 offset)
  {
    foreach (GameObject bg in allTiles) {
      Vector3 pos = bg.transform.position;
      pos += offset;
      bg.transform.position = pos;
    }
  }
}