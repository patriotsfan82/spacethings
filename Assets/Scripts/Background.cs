﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour{

  // The max number of backgrounds to be rendered at any one time is 4
  // but we can have more than that in the list. This class will manage
  // the items and keep track of which ones are currently in use/being rendered and such.
  public List<GameObject> backgroundTiles;


}
