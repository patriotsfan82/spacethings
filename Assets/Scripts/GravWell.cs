﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravWell : MonoBehaviour {

	// Use this for initialization
	private void Start () {
    GameManager.instance.RegisterGravWell (this);
	}

  // Get the Vector2 Acceleration value 
  public Vector2 GetGravityAcceleration(Vector2 point)
  {
    Vector2 accelDir = new Vector2 (transform.position.x, transform.position.y) - point; 
    float gravDistance = accelDir.magnitude;
    PointEffector2D effector = GetComponent<PointEffector2D> ();
    if (effector != null) {
      // Note that effector force is negative, but we want it positive
      float accelStrength = (-1.0f * effector.forceMagnitude) / (gravDistance * gravDistance);
      return accelDir.normalized * accelStrength;
    } else
      return new Vector2 (0f, 0f);
  }
}
