﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shippy : MonoBehaviour {

  public float maxAbsVelocity;
  public float acceleration;
  public Text energyValText;
  public Text speedValText;

  private Vector2 curVelocity = new Vector2 (0f, 0f);
  private Vector2 curAcceleration = new Vector2 (0f, 0f);
  private BoxCollider2D boxCollider;
  private Rigidbody2D rb2D = null;
  private LineRenderer line = null;

  private bool mouseClicked = false;
  private bool mouseReleased = false;
  private Vector3 mouseVector = new Vector3 (0f, 0f, 0f);
  private Vector3 lastPos = new Vector3 (0f, 0f, 0f);

  private float energyLevel = 50.0f;
  private const float energyLevelMax = 100.0f;
  private const float energyLevelMin = 0.0f;
  private const float energyRate = 3.0f;
  private const float inverseEnergyRate = 1 / energyRate;

  private const float gravStrengthToEnergy = 0.005f;

  private void Start ()
  {
    // Get a component reference to this object's BoxCollider2D
    boxCollider = GetComponent <BoxCollider2D> ();

    // Get a component reference to this object's Rigidbody2D
    rb2D = GetComponent <Rigidbody2D> ();
    line = GetComponent <LineRenderer> ();

    line.numPositions = 200;
    lastPos = transform.position;
  }

  private void Update()
  {
    if (Input.GetMouseButtonUp (0)) {
      mouseReleased = true;
    }

    if (Input.GetMouseButton(0)) {
      mouseClicked = true;

      Vector3 trueMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
      trueMouse.z = 0f;
      mouseVector = trueMouse - transform.position;

      if (line != null) {
//        line.SetPosition (0, transform.position);
//        line.SetPosition (1, trueMouse);

        Vector2 curVelocity = rb2D.velocity;
        Vector2 position = new Vector2(transform.position.x, transform.position.y);

        // Manually calculate effect of impulse
        Vector2 accel = new Vector2(mouseVector.x, mouseVector.y);
        line.SetPosition (0, new Vector3 (position.x, position.y, 0f));

        accel *= 0.5f; // Currently applying this factor in the fixedUpdate

        // This needs to use up some amount of energy
        // Lets make sure we aren't using all
        float energyUsed = accel.magnitude * energyRate;
        if (energyUsed > energyLevel) {
          accel.Normalize();
          accel *= energyLevel * inverseEnergyRate;
          energyUsed = energyLevel;
        }

        energyValText.text = (energyLevel - energyUsed).ToString("#0.0") + "%";
        energyValText.color = Color.red;

        // So for first deltatime... smooth acceleration 
//        position += (curVelocity * Time.fixedDeltaTime) + (Time.fixedDeltaTime * Time.fixedDeltaTime * 0.5f * accel);
//        curVelocity += accel * Time.fixedDeltaTime;


        // First time step...
        // Lets talk through this.
        // On frame 0, Impulse would take affect. This impulse is what? An increase to the velocity applied then but
        // not affecting movement until next frame?
        curVelocity += accel;

        // Additionally,from previous point, there are planetary gravity effects.
        // These effects will be an acceleration based change to position
        // Additionally, velocity for next time will be increased

        // Update position and velocity based on nearby gravity sources
        Vector2 gravAccel = new Vector2 (0f, 0f);
        List<GravWell> gravWells = GameManager.instance.GravWellsNearPoint(new Vector3(position.x, position.y, 0f));
        foreach (GravWell gravWell in gravWells) {
          gravAccel += gravWell.GetGravityAcceleration (position);
        }

        // Should have calculated all effects now, time to update position and velocity
        // First, adjust position based on velocity at start of calculation
        position += curVelocity * Time.fixedDeltaTime;

        // Then, update position and velocity based on gravitational effects
        position += (Time.fixedDeltaTime * Time.fixedDeltaTime * 0.5f * gravAccel);
        curVelocity += Time.fixedDeltaTime * gravAccel;
         
        line.SetPosition (1, new Vector3 (position.x, position.y, 0f));

        // How many delta time iterations to predict
        for (int i = 2; i < 200; i++) {

          // Reset these values
          gravAccel = new Vector2 (0f, 0f);

          // Update position and velocity based on acceleration
          gravWells = GameManager.instance.GravWellsNearPoint(new Vector3(position.x, position.y, 0f));
          foreach (GravWell gravWell in gravWells) {
            gravAccel += gravWell.GetGravityAcceleration (position);
          }

          // Should have calculated all effects now, time to update position and velocity
          // First, adjust position based on velocity at start of calculation
          position += curVelocity * Time.fixedDeltaTime;

          // Then, update position and velocity based on gravitational effects
          position += (Time.fixedDeltaTime * Time.fixedDeltaTime * 0.5f * gravAccel);
          curVelocity += Time.fixedDeltaTime * gravAccel;
            
          line.SetPosition(i,  new Vector3 (position.x, position.y, 0f));
        }

        line.enabled = true;
      }
    } else {
      mouseClicked = false;

      // Update energy text here...
      energyValText.text = energyLevel.ToString("#0.0") + "%";
      energyValText.color = Color.white;

      if (line != null) {
 //       line.enabled = false;

        Vector2 curVelocity = rb2D.velocity;
        Vector2 position = new Vector2(rb2D.position.x, rb2D.position.y);
        line.SetPosition (0, new Vector3 (position.x, position.y, 0f));

        // Manually calculate effect of impulse
        Vector2 accel = new Vector2(mouseVector.x, mouseVector.y);
        line.SetPosition (0, new Vector3 (position.x, position.y, 0f));

        if (mouseReleased) {
          accel *= 0.5f; // Currently applying this factor in the fixedUpdate
          curVelocity += accel;
        }

        // Update position and velocity based on nearby gravity sources
        Vector2 gravAccel = new Vector2 (0f, 0f);
        List<GravWell> gravWells = GameManager.instance.GravWellsNearPoint(new Vector3(position.x, position.y, 0f));
        foreach (GravWell gravWell in gravWells) {
          gravAccel += gravWell.GetGravityAcceleration (position);
        }
          
        // Then, update position and velocity based on gravitational effects
        position += (curVelocity * Time.fixedDeltaTime) + (Time.fixedDeltaTime * Time.fixedDeltaTime * 0.5f * gravAccel);
        curVelocity += Time.fixedDeltaTime * gravAccel;

        line.SetPosition (1, new Vector3 (position.x, position.y, 0f));

        // How many delta time iterations to predict
        for (int i = 2; i < 200; i++) {

          // Reset these values
          gravAccel = new Vector2 (0f, 0f);

          // Update position and velocity based on acceleration
          gravWells = GameManager.instance.GravWellsNearPoint(new Vector3(position.x, position.y, 0f));
          foreach (GravWell gravWell in gravWells) {
            gravAccel += gravWell.GetGravityAcceleration (position);
          }

          // Should have calculated all effects now, time to update position and velocity
          // First, adjust position based on velocity at start of calculation
          position += (curVelocity * Time.fixedDeltaTime) + (Time.fixedDeltaTime * Time.fixedDeltaTime * 0.5f * gravAccel);
          curVelocity += Time.fixedDeltaTime * gravAccel;

          line.SetPosition(i,  new Vector3 (position.x, position.y, 0f));
        }
      }
    }
      
    // Parallax Planet control....
    BackgroundManager.instance.PlayerMoved (lastPos, transform.position);

    lastPos = transform.localPosition;

    speedValText.text = rb2D.velocity.magnitude.ToString ("#0.0") + " km/s";

    // Turn ship to face direction of acceleration (NOT impacted by gravity)
    // Should look into this math more.
    float angle = Mathf.Atan2 (rb2D.velocity.x, rb2D.velocity.y) * Mathf.Rad2Deg;
    this.transform.rotation = Quaternion.AngleAxis (angle, Vector3.back);
  }

  private void FixedUpdate()
  {
    Vector2 accelerate = new Vector2 (0f, 0f);

//    if (rb2D.velocity.magnitude > maxAbsVelocity) {
//      // If the velocity is greater than or equal to the max velocity, lets clamp
//      // it for now.
//      Vector2 currentVel = rb2D.velocity;
//      rb2D.velocity = currentVel.normalized * maxAbsVelocity;
//    }

    // Calculate energy gain
    Vector2 gravStrength = new Vector2 (0f, 0f);
    List<GravWell> gravWells = GameManager.instance.GravWellsNearPoint(new Vector3(rb2D.position.x, rb2D.position.y, 0f));
    foreach (GravWell gravWell in gravWells) {
      gravStrength += gravWell.GetGravityAcceleration (new Vector2(rb2D.position.x, rb2D.position.y));
    }

    energyLevel += gravStrength.magnitude * gravStrengthToEnergy;

    if (energyLevel > energyLevelMax)
      energyLevel = energyLevelMax;

    // Take user input into account
    if (mouseReleased)
//    if (mouseClicked)
    {
      mouseReleased = false;

      // Mouse was released, lets boost off!
      // Max velocity poses a problem always.. ignore for now
      accelerate.x = mouseVector.x * 0.5f; // Multiply by some factor?
      accelerate.y = mouseVector.y * 0.5f; // Multiply by some factor?

      // This needs to use up some amount of energy
      // Lets make sure we aren't using all
      float energyUsed = accelerate.magnitude * energyRate;
      if (energyUsed > energyLevel) {
        accelerate.Normalize ();
        accelerate *= energyLevel * inverseEnergyRate;
        energyUsed = energyLevel;
      }
       
      energyLevel -= energyUsed;
       
      if (accelerate.magnitude > float.Epsilon) {
        rb2D.AddForce (accelerate, ForceMode2D.Impulse);
      }
    } 
  }
}